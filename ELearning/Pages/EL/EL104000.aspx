<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormView.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="EL104000.aspx.cs" Inherits="Page_EL104000" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/MasterPages/FormView.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" Runat="Server">
	<px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%" PrimaryView="Courses" TypeName="ELearning.CourseMaint">
	</px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" Runat="Server">
	<px:PXFormView ID="form" runat="server" DataSourceID="ds" Style="z-index: 100" Width="100%" DataMember="Courses" TabIndex="1200">
		<Template>
			<px:PXLayoutRule runat="server" StartRow="True"/>
		    <px:PXSegmentMask ID="edCourseID" runat="server" DataField="CourseID">
            </px:PXSegmentMask>
            <px:PXTextEdit ID="edCourseName" runat="server" AlreadyLocalized="False" DataField="CourseName" IsClientControl="True">
            </px:PXTextEdit>
            <px:PXNumberEdit ID="edCreditUnits" runat="server" AlreadyLocalized="False" DataField="CreditUnits" IsClientControl="True">
            </px:PXNumberEdit>
            <px:PXCheckBox ID="edStatus" runat="server" AlreadyLocalized="False" DataField="Status" ForceServerRendering="False" IsClientControl="True" Text="Status">
            </px:PXCheckBox>
		</Template>
		<AutoSize Container="Window" Enabled="True" MinHeight="200" />
	</px:PXFormView>
</asp:Content>
