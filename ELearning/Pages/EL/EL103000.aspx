<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormView.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="EL103000.aspx.cs" Inherits="Page_EL103000" Title="Untitled Page" %>
<%@ MasterType VirtualPath="~/MasterPages/FormView.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" Runat="Server">
	<px:PXDataSource ID="ds" runat="server" Visible="True" Width="100%" PrimaryView="Programmes" TypeName="ELearning.ProgrammeMaint">
	</px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" Runat="Server">
	<px:PXFormView ID="form" runat="server" DataSourceID="ds" Style="z-index: 100" Width="100%" DataMember="Programmes" TabIndex="1200">
		<Template>
			<px:PXLayoutRule runat="server" StartRow="True"/>
		    <px:PXSegmentMask ID="edProgrammeID" runat="server" DataField="ProgrammeID">
            </px:PXSegmentMask>
            <px:PXTextEdit ID="edProgrammeName" runat="server" AlreadyLocalized="False" DataField="ProgrammeName" IsClientControl="True">
            </px:PXTextEdit>
            <px:PXNumberEdit ID="edRequiredCreditUnits" runat="server" AlreadyLocalized="False" DataField="RequiredCreditUnits" IsClientControl="True">
            </px:PXNumberEdit>
            <px:PXNumberEdit ID="edDuration" runat="server" AlreadyLocalized="False" DataField="Duration" IsClientControl="True">
            </px:PXNumberEdit>
            <px:PXDropDown ID="edDurationUnit" runat="server" DataField="DurationUnit">
            </px:PXDropDown>
            <px:PXCheckBox ID="edStatus" runat="server" AlreadyLocalized="False" DataField="Status" ForceServerRendering="False" IsClientControl="True" Text="Status">
            </px:PXCheckBox>
		</Template>
		<AutoSize Container="Window" Enabled="True" MinHeight="200" />
	</px:PXFormView>
</asp:Content>
