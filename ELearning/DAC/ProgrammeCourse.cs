namespace ELearning
{
	using System;
	using PX.Data;
	using PX.Objects.GL;
	public class ProgrammeCourse : PX.Data.IBqlTable
	{
		#region ProgrammeCourseID
		public abstract class programmeCourseID : PX.Data.IBqlField
		{
		}
		protected int _ProgrammeCourseID;
		[PXDBIdentity]
		[PXUIField(DisplayName = "ProgrammeCourse ID")]
		
		public virtual int ProgrammeCourseID
		{
			get
			{
				return this._ProgrammeCourseID;
			}
			set
			{
				this._ProgrammeCourseID = value;
			}
		}
		#endregion
		#region ProgrammeID
		public abstract class programmeID : PX.Data.IBqlField
		{
		}
		protected string _ProgrammeID;
		[PXDBString(13, IsFixed = true, IsKey = true)]
		[PXDefault(typeof(Programme.programmeID))]
		[PXParent(typeof(Select<Programme, Where<Programme.programmeID, Equal<Current<ProgrammeCourse.programmeID>>>>))]
		[PXUIField(DisplayName = "Programme ID")]
		[PXDimensionSelector("PROGRAMME",
			typeof(Search<Programme.programmeID>))]
		public virtual string ProgrammeID
		{
			get
			{
				return this._ProgrammeID;
			}
			set
			{
				this._ProgrammeID = value;
			}
		}
		#endregion
		#region CourseID
		public abstract class courseID : PX.Data.IBqlField
		{
		}
		protected string _CourseID;
		[PXDBString(9, IsFixed = true, IsKey = true)]
		[PXDefault()]
		[PXUIField(DisplayName = "Prerequisite Course")]
		[PXDimensionSelector("COURSE",
			typeof(Search<Course.courseID>))]
		public virtual string CourseID
		{
			get
			{
				return this._CourseID;
			}
			set
			{
				this._CourseID = value;
			}
		}
		#endregion
		#region TermID
		public abstract class termID : PX.Data.IBqlField
		{
		}
		protected string _TermID;
		[PXDBString(8)]
		[PXUIField(DisplayName = "Term ID")]
		public virtual string TermID
		{
			get
			{
				return this._TermID;
			}
			set
			{
				this._TermID = value;
			}
		}
		#endregion
		#region Status
		public abstract class status : PX.Data.IBqlField
		{
		}
		protected bool? _Status;
		[PXDBBool()]
		[PXUIField(DisplayName = "Status")]
		public virtual bool? Status
		{
			get
			{
				return this._Status;
			}
			set
			{
				this._Status = value;
			}
		}
		#endregion
		#region NoteID
		public abstract class noteID : PX.Data.IBqlField
		{
		}
		[PXNote()]
		public virtual Guid? NoteID { get; set; }
		#endregion
		#region tstamp
		public abstract class Tstamp : PX.Data.BQL.BqlByteArray.Field<Tstamp> { }
		protected Byte[] _tstamp;
		[PXDBTimestamp()]
		public virtual Byte[] tstamp
		{
			get
			{
				return this._tstamp;
			}
			set
			{
				this._tstamp = value;
			}
		}
		#endregion#region tstamp
		#region CreatedByID
		public abstract class createdByID : PX.Data.IBqlField
		{
		}
		protected Guid? _CreatedByID;
		[PXDBCreatedByID()]
		[PXUIField(DisplayName = "Created By ID")]
		public virtual Guid? CreatedByID
		{
			get
			{
				return this._CreatedByID;
			}
			set
			{
				this._CreatedByID = value;
			}
		}
		#endregion
		#region CreatedByScreenID
		public abstract class createdByScreenID : PX.Data.IBqlField
		{
		}
		protected string _CreatedByScreenID;
		[PXDBCreatedByScreenID()]
		[PXUIField(DisplayName = "Created By Screen ID")]
		public virtual string CreatedByScreenID
		{
			get
			{
				return this._CreatedByScreenID;
			}
			set
			{
				this._CreatedByScreenID = value;
			}
		}
		#endregion
		#region CreatedDateTime
		public abstract class createdDateTime : PX.Data.IBqlField
		{
		}
		protected DateTime? _CreatedDateTime;
		[PXDBCreatedDateTime()]
		[PXUIField(DisplayName = "Created Date Time")]
		public virtual DateTime? CreatedDateTime
		{
			get
			{
				return this._CreatedDateTime;
			}
			set
			{
				this._CreatedDateTime = value;
			}
		}
		#endregion
		#region LastModifiedByID
		public abstract class lastModifiedByID : PX.Data.IBqlField
		{
		}
		protected Guid? _LastModifiedByID;
		[PXDBLastModifiedByID()]
		[PXUIField(DisplayName = "Last Modified By ID")]
		public virtual Guid? LastModifiedByID
		{
			get
			{
				return this._LastModifiedByID;
			}
			set
			{
				this._LastModifiedByID = value;
			}
		}
		#endregion
		#region LastModifiedByScreenID
		public abstract class lastModifiedByScreenID : PX.Data.IBqlField
		{
		}
		protected string _LastModifiedByScreenID;
		[PXDBLastModifiedByScreenID()]
		[PXUIField(DisplayName = "Last Modified By Screen ID")]
		public virtual string LastModifiedByScreenID
		{
			get
			{
				return this._LastModifiedByScreenID;
			}
			set
			{
				this._LastModifiedByScreenID = value;
			}
		}
		#endregion
		#region LastModifiedDateTime
		public abstract class lastModifiedDateTime : PX.Data.IBqlField
		{
		}
		protected DateTime? _LastModifiedDateTime;
		[PXDBLastModifiedDateTime()]
		[PXUIField(DisplayName = "Last Modified DateTime")]
		public virtual DateTime? LastModifiedDateTime
		{
			get
			{
				return this._LastModifiedDateTime;
			}
			set
			{
				this._LastModifiedDateTime = value;
			}
		}
		#endregion
	}
}