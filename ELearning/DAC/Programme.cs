namespace ELearning
{
	using System;
	using PX.Data;
	using PX.Objects.GL;
	public class Programme : PX.Data.IBqlTable
	{
		#region ProgrammeID
		public abstract class programmeID : PX.Data.IBqlField
		{
		}
		protected string _ProgrammeID;
		[PXDBString(13, IsFixed = true, IsKey = true)]
		[PXDefault()]
		[PXUIField(DisplayName = "Programme ID")]
		[PXDimension("PROGRAMME", ValidComboRequired = false)]
		//[ProgrammeRaw]
		public virtual string ProgrammeID
		{
			get
			{
				return this._ProgrammeID;
			}
			set
			{
				this._ProgrammeID = value;
			}
		}
		#endregion
		#region ProgrammeName
		public abstract class programmeName : PX.Data.IBqlField
		{
		}
		protected string _ProgrammeName;
		[PXDBString(128, IsUnicode = true)]
		[PXUIField(DisplayName = "ProgrammeName")]
		public virtual string ProgrammeName
		{
			get
			{
				return this._ProgrammeName;
			}
			set
			{
				this._ProgrammeName = value;
			}
		}
		#endregion
		#region RequiredCreditUnits
		public abstract class requiredCreditUnits : PX.Data.IBqlField
		{
		}
		protected decimal? _RequiredCreditUnits;
		[PXDBDecimal(2)]
		[PXUIField(DisplayName = "Required Credit Units")]
		public virtual decimal? RequiredCreditUnits
		{
			get
			{
				return this._RequiredCreditUnits;
			}
			set
			{
				this._RequiredCreditUnits = value;
			}
		}
		#endregion
		#region Duration
		public abstract class duration : PX.Data.IBqlField
		{
		}
		protected int? _Duration;
		[PXDBInt()]
		[PXUIField(DisplayName = "Duration")]
		public virtual int? Duration
		{
			get
			{
				return this._Duration;
			}
			set
			{
				this._Duration = value;
			}
		}
		#endregion
		#region DurationUnit
		public abstract class durationUnit : PX.Data.IBqlField
		{
		}
		protected string _DurationUnit;
		[PXDBString(8)]
		[PXStringList(
		new string[]
		{
			"Month",
			"Day",
			"Year"
		},
		new string[]
		{
			"Month",
			"Day",
			"Year"
		})]
		[PXUIField(DisplayName = "Duration Unit")]
		public virtual string DurationUnit
		{
			get
			{
				return this._DurationUnit;
			}
			set
			{
				this._DurationUnit = value;
			}
		}
		#endregion
		#region Status
		public abstract class status : PX.Data.IBqlField
		{
		}
		protected bool? _Status;
		[PXDBBool()]
		[PXUIField(DisplayName = "Status")]
		public virtual bool? Status
		{
			get
			{
				return this._Status;
			}
			set
			{
				this._Status = value;
			}
		}
		#endregion
		#region NoteID
		public abstract class noteID : PX.Data.IBqlField
		{
		}
		[PXNote()]
		public virtual Guid? NoteID { get; set; }
		#endregion
		#region tstamp
		public abstract class Tstamp : PX.Data.BQL.BqlByteArray.Field<Tstamp> { }
		protected Byte[] _tstamp;
		[PXDBTimestamp()]
		public virtual Byte[] tstamp
		{
			get
			{
				return this._tstamp;
			}
			set
			{
				this._tstamp = value;
			}
		}
		#endregion#region tstamp
		#region CreatedByID
		public abstract class createdByID : PX.Data.IBqlField
		{
		}
		protected Guid? _CreatedByID;
		[PXDBCreatedByID()]
		[PXUIField(DisplayName = "Created By ID")]
		public virtual Guid? CreatedByID
		{
			get
			{
				return this._CreatedByID;
			}
			set
			{
				this._CreatedByID = value;
			}
		}
		#endregion
		#region CreatedByScreenID
		public abstract class createdByScreenID : PX.Data.IBqlField
		{
		}
		protected string _CreatedByScreenID;
		[PXDBCreatedByScreenID()]
		[PXUIField(DisplayName = "Created By Screen ID")]
		public virtual string CreatedByScreenID
		{
			get
			{
				return this._CreatedByScreenID;
			}
			set
			{
				this._CreatedByScreenID = value;
			}
		}
		#endregion
		#region CreatedDateTime
		public abstract class createdDateTime : PX.Data.IBqlField
		{
		}
		protected DateTime? _CreatedDateTime;
		[PXDBCreatedDateTime()]
		[PXUIField(DisplayName = "Created Date Time")]
		public virtual DateTime? CreatedDateTime
		{
			get
			{
				return this._CreatedDateTime;
			}
			set
			{
				this._CreatedDateTime = value;
			}
		}
		#endregion
		#region LastModifiedByID
		public abstract class lastModifiedByID : PX.Data.IBqlField
		{
		}
		protected Guid? _LastModifiedByID;
		[PXDBLastModifiedByID()]
		[PXUIField(DisplayName = "Last Modified By ID")]
		public virtual Guid? LastModifiedByID
		{
			get
			{
				return this._LastModifiedByID;
			}
			set
			{
				this._LastModifiedByID = value;
			}
		}
		#endregion
		#region LastModifiedByScreenID
		public abstract class lastModifiedByScreenID : PX.Data.IBqlField
		{
		}
		protected string _LastModifiedByScreenID;
		[PXDBLastModifiedByScreenID()]
		[PXUIField(DisplayName = "Last Modified By Screen ID")]
		public virtual string LastModifiedByScreenID
		{
			get
			{
				return this._LastModifiedByScreenID;
			}
			set
			{
				this._LastModifiedByScreenID = value;
			}
		}
		#endregion
		#region LastModifiedDateTime
		public abstract class lastModifiedDateTime : PX.Data.IBqlField
		{
		}
		protected DateTime? _LastModifiedDateTime;
		[PXDBLastModifiedDateTime()]
		[PXUIField(DisplayName = "Last Modified DateTime")]
		public virtual DateTime? LastModifiedDateTime
		{
			get
			{
				return this._LastModifiedDateTime;
			}
			set
			{
				this._LastModifiedDateTime = value;
			}
		}
		#endregion
	}
}