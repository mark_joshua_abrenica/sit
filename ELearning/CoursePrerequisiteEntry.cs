using PX.Data;
using PX.SM;
using System;
using System.Collections;
using System.Collections.Generic;


namespace ELearning
{
    public class CoursePrerequisiteEntry : PXGraph<CoursePrerequisiteEntry, Course>
    {

        public PXSelect<Course> Courses;
        public PXSelect<CoursePrerequisite, Where<CoursePrerequisite.courseID, Equal<Current<Course.courseID>>>> CoursePrerequisites;
        #region Events
        [PXDBString(9, IsFixed = true, IsKey = true)]
        [PXDefault()]
        [PXUIField(DisplayName = "Course ID")]
        [PXDimensionSelector("COURSE",
            typeof(Search<Course.courseID>))]
        protected void Course_CourseID_CacheAttached(PXCache sender)
        {
        }
        [PXDBString(9, IsFixed = true, IsKey = true)]
        [PXDefault()]
        [PXUIField(DisplayName = "Prerequisite Course")]
        [PXDimensionSelector("COURSE",
            typeof(Search<Course.courseID>))]
        protected void CoursePrerequisite_RequiredCourseID_CacheAttached(PXCache sender)
        {
        }
        #endregion
    }
}