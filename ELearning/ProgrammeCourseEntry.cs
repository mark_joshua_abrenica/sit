using PX.Data;
using PX.SM;
using System;
using System.Collections;
using System.Collections.Generic;


namespace ELearning
{
    public class ProgrammeCourseEntry : PXGraph<ProgrammeCourseEntry, Programme>
    {

        public PXSelect<Programme> Programmes;
        public PXSelect<ProgrammeCourse, Where<ProgrammeCourse.programmeID, Equal<Current<Programme.programmeID>>>> ProgrammeCourses;
        #region Events
        [PXDBString(13, IsFixed = true, IsKey = true)]
        [PXDefault()]
        [PXUIField(DisplayName = "Programme ID")]
        [PXDimensionSelector("PROGRAMME",
            typeof(Search<Programme.programmeID>))]
        protected void Programme_ProgrammeID_CacheAttached(PXCache sender)
        {
        }
        #endregion
    }
}