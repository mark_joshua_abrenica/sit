SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE  [dbo].[Course](
[CompanyID] [int] NOT NULL
,[CourseID] [CHAR](9) NOT NULL
,[CourseName] nvarchar(128)

,[CreditUnits] decimal(4,2)
,[Status] bit --1 = Active;  0 = InActive

,[NoteID] [uniqueidentifier]
,[tstamp] [timestamp] NULL
,[CreatedByID] [uniqueidentifier] NOT NULL
,[CreatedByScreenID] [char](8) NOT NULL
,[CreatedDateTime] [datetime] NOT NULL
,[LastModifiedByID] [uniqueidentifier] NOT NULL
,[LastModifiedByScreenID] [char](8) NOT NULL
,[LastModifiedDateTime] [datetime] NOT NULL


CONSTRAINT [Course_PK] PRIMARY KEY CLUSTERED
(
[CompanyID] ASC
,[CourseID]
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF,
ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Course] ADD DEFAULT ((0)) FOR [CompanyID]
GO