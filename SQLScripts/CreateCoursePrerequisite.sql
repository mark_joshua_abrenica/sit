SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE  [dbo].[CoursePrerequisite](
[CompanyID] [int] NOT NULL
,[CoursePrerequisiteID] INT IDENTITY(1,1)
,[CourseID] [CHAR](9) NOT NULL
,[RequiredCourseID] [CHAR](9) NOT NULL
,[EffectivityStartDate] DATE
,[EffectivityEndDate] DATE

,[Status] bit --1 = Active;  0 = InActive

,[NoteID] [uniqueidentifier]
,[tstamp] [timestamp] NULL
,[CreatedByID] [uniqueidentifier] NOT NULL
,[CreatedByScreenID] [char](8) NOT NULL
,[CreatedDateTime] [datetime] NOT NULL
,[LastModifiedByID] [uniqueidentifier] NOT NULL
,[LastModifiedByScreenID] [char](8) NOT NULL
,[LastModifiedDateTime] [datetime] NOT NULL


CONSTRAINT [CoursePrerequisite_PK] PRIMARY KEY CLUSTERED
(
[CompanyID] ASC
,[CoursePrerequisiteID]
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF,
ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[CoursePrerequisite] ADD DEFAULT ((0)) FOR [CompanyID]
GO