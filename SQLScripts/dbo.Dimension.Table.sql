USE [PracticeSit]
GO

INSERT [Dimension] ([CompanyID], [DimensionID], [Descr], [Length], [Segments], [Internal], [NumberingID], [Validate], [LookupMode], [SpecificModule], [ParentDimensionID], [TableName], [FieldName], [DividerField], [DividerValue], [CreatedByID], [CreatedByScreenID], [CreatedDateTime], [LastModifiedByID], [LastModifiedByScreenID], [LastModifiedDateTime], [CompanyMask]) VALUES (2, N'PROGRAMME', N'Programme ID', 13, 5, 0, NULL, 1, N'K0', NULL, NULL, NULL, NULL, NULL, NULL, N'b5344897-037e-4d58-b5c3-1bdfd0f47bf9', N'CS202000', CAST(N'2021-10-05T07:25:27.570' AS DateTime), N'b5344897-037e-4d58-b5c3-1bdfd0f47bf9', N'CS202000', CAST(N'2021-10-05T11:11:40.283' AS DateTime), 0xAA)
INSERT [Dimension] ([CompanyID], [DimensionID], [Descr], [Length], [Segments], [Internal], [NumberingID], [Validate], [LookupMode], [SpecificModule], [ParentDimensionID], [TableName], [FieldName], [DividerField], [DividerValue], [CreatedByID], [CreatedByScreenID], [CreatedDateTime], [LastModifiedByID], [LastModifiedByScreenID], [LastModifiedDateTime], [CompanyMask]) VALUES (2, N'COURSE', N'Course', 9, 4, 0, NULL, 1, N'K0', NULL, NULL, NULL, NULL, NULL, NULL, N'b5344897-037e-4d58-b5c3-1bdfd0f47bf9', N'CS202000', CAST(N'2021-10-05T12:25:04.317' AS DateTime), N'b5344897-037e-4d58-b5c3-1bdfd0f47bf9', N'CS202000', CAST(N'2021-10-05T12:29:03.857' AS DateTime), 0xAA)
GO

