USE [PracticeSit]
GO


DELETE FROM [Segment] WHERE [CompanyID] = 2 AND [DimensionID] = N'COURSE' AND [SegmentID] = 1
DELETE FROM [Segment] WHERE [CompanyID] = 2 AND [DimensionID] = N'COURSE' AND [SegmentID] = 2
DELETE FROM [Segment] WHERE [CompanyID] = 2 AND [DimensionID] = N'COURSE' AND [SegmentID] = 3
DELETE FROM [Segment] WHERE [CompanyID] = 2 AND [DimensionID] = N'COURSE' AND [SegmentID] = 4
DELETE FROM [Segment] WHERE [CompanyID] = 2 AND [DimensionID] = N'PROGRAMME' AND [SegmentID] = 1
DELETE FROM [Segment] WHERE [CompanyID] = 2 AND [DimensionID] = N'PROGRAMME' AND [SegmentID] = 2
DELETE FROM [Segment] WHERE [CompanyID] = 2 AND [DimensionID] = N'PROGRAMME' AND [SegmentID] = 3
DELETE FROM [Segment] WHERE [CompanyID] = 2 AND [DimensionID] = N'PROGRAMME' AND [SegmentID] = 4
DELETE FROM [Segment] WHERE [CompanyID] = 2 AND [DimensionID] = N'PROGRAMME' AND [SegmentID] = 5
GO

GO

