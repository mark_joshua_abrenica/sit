SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE  [dbo].[ProgrammeCourse](
[CompanyID] [int] NOT NULL
,[ProgrammeCourseID] INT IDENTITY(1,1)
,[ProgrammeID] [CHAR](13) NOT NULL
,[CourseID] [CHAR](9) NOT NULL
,[TermID] nvarchar(8) 

,[Status] bit --1 = Active;  0 = InActive

,[NoteID] [uniqueidentifier]
,[tstamp] [timestamp] NULL
,[CreatedByID] [uniqueidentifier] NOT NULL
,[CreatedByScreenID] [char](8) NOT NULL
,[CreatedDateTime] [datetime] NOT NULL
,[LastModifiedByID] [uniqueidentifier] NOT NULL
,[LastModifiedByScreenID] [char](8) NOT NULL
,[LastModifiedDateTime] [datetime] NOT NULL


CONSTRAINT [ProgrammeCourse_PK] PRIMARY KEY CLUSTERED
(
[CompanyID] ASC
,[ProgrammeCourseID]
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF,
ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[ProgrammeCourse] ADD DEFAULT ((0)) FOR [CompanyID]
GO