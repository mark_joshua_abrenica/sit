<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormDetail.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="EL201000.aspx.cs" Inherits="Page_EL201000" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/MasterPages/FormDetail.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" runat="Server">
    <px:PXDataSource ID="ds" runat="server" Visible="True" TypeName="ELearning.CoursePrerequisiteEntry" PrimaryView="Courses" SuspendUnloading="False">
        <CallbackCommands>
   
     </CallbackCommands>
    </px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" runat="Server">
    <px:PXFormView ID="form" runat="server" DataSourceID="ds" Style="z-index: 100" Width="100%" DataMember="Courses" TabIndex="100">
        <Template>
            <px:PXLayoutRule runat="server" StartRow="True" StartColumn="True" />
            <px:PXSelector ID="edCourseID" runat="server" DataField="CourseID">
            </px:PXSelector>
            
            <px:PXTextEdit ID="edCourseName" runat="server" AlreadyLocalized="False" DataField="CourseName" IsClientControl="True">
            </px:PXTextEdit>
            <px:PXNumberEdit ID="edCreditUnits" runat="server" AlreadyLocalized="False" DataField="CreditUnits" IsClientControl="True">
            </px:PXNumberEdit>
            <px:PXCheckBox ID="edStatus" runat="server" AlreadyLocalized="False" DataField="Status" ForceServerRendering="False" IsClientControl="True" Text="Status">
            </px:PXCheckBox>
            
        </Template>
        <AutoSize Container="Window" Enabled="True" MinHeight="100" />
    </px:PXFormView>
</asp:Content>
<asp:Content ID="cont3" ContentPlaceHolderID="phG" runat="Server">
     <px:PXTab ID="tab" runat="server" Height="840px" Style="z-index: 100;" Width="100%">
        <Items>
            <px:PXTabItem Text="Course Prerequisites" BindingContext="form" RepaintOnDemand="false">
                <Template>
                    <px:PXGrid ID="gridCoursePrerequisite" runat="server" DataSourceID="ds" Width="100%" SkinID="DetailsInTab" NoteIndicator="True" Style="left: 0px; top: 0px;" TabIndex="600" SyncPosition="True">
                        <AutoSize Enabled="True" />
                        <Mode InitNewRow="True" ></Mode>
                        
                        <Levels>
                            <px:PXGridLevel DataMember="CoursePrerequisites">
                                <RowTemplate>
                                    <px:PXSegmentMask ID="edRequiredCourseID" runat="server" DataField="RequiredCourseID">
                                    </px:PXSegmentMask>
                                    <px:PXDateTimeEdit ID="edEffectivityStartDate" runat="server" AlreadyLocalized="False" DataField="EffectivityStartDate" IsClientControl="True">
                                    </px:PXDateTimeEdit>
                                    <px:PXDateTimeEdit ID="edEffectivityEndDate" runat="server" AlreadyLocalized="False" DataField="EffectivityEndDate" IsClientControl="True">
                                    </px:PXDateTimeEdit>
                                    <px:PXCheckBox ID="edStatus" runat="server" AlreadyLocalized="False" DataField="Status" ForceServerRendering="False" IsClientControl="True" Text="Status">
                                    </px:PXCheckBox>
                                </RowTemplate>
                                <Columns>
                                    <px:PXGridColumn DataField="RequiredCourseID" Width="140px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="EffectivityStartDate" Width="90px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="EffectivityEndDate" Width="90px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="Status" TextAlign="Center" Type="CheckBox" Width="60px">
                                    </px:PXGridColumn>
                                </Columns>
                            </px:PXGridLevel>
                        </Levels>
                    </px:PXGrid>
                </Template>
            </px:PXTabItem>
        </Items>
        </px:PXTab>
	</asp:Content>