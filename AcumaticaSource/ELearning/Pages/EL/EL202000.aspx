<%@ Page Language="C#" MasterPageFile="~/MasterPages/FormDetail.master" AutoEventWireup="true" ValidateRequest="false" CodeFile="EL202000.aspx.cs" Inherits="Page_EL202000" Title="Untitled Page" %>

<%@ MasterType VirtualPath="~/MasterPages/FormDetail.master" %>

<asp:Content ID="cont1" ContentPlaceHolderID="phDS" runat="Server">
    <px:PXDataSource ID="ds" runat="server" Visible="True" TypeName="ELearning.ProgrammeCourseEntry" PrimaryView="Programmes" SuspendUnloading="False">
        <CallbackCommands>
   
     </CallbackCommands>
    </px:PXDataSource>
</asp:Content>
<asp:Content ID="cont2" ContentPlaceHolderID="phF" runat="Server">
    <px:PXFormView ID="form" runat="server" DataSourceID="ds" Style="z-index: 100" Width="100%" DataMember="Programmes" TabIndex="100">
        <Template>
            <px:PXLayoutRule runat="server" StartRow="True" StartColumn="True" />
            <px:PXSegmentMask ID="edProgrammeID" runat="server" DataField="ProgrammeID">
            </px:PXSegmentMask>
            <px:PXTextEdit ID="edProgrammeName" runat="server" AlreadyLocalized="False" DataField="ProgrammeName" IsClientControl="True">
            </px:PXTextEdit>
            <px:PXNumberEdit ID="edRequiredCreditUnits" runat="server" AlreadyLocalized="False" DataField="RequiredCreditUnits" IsClientControl="True">
            </px:PXNumberEdit>
            <px:PXNumberEdit ID="edDuration" runat="server" AlreadyLocalized="False" DataField="Duration" IsClientControl="True">
            </px:PXNumberEdit>
            <px:PXDropDown ID="edDurationUnit" runat="server" DataField="DurationUnit">
            </px:PXDropDown>
            
            <px:PXCheckBox ID="edStatus" runat="server" AlreadyLocalized="False" DataField="Status" ForceServerRendering="False" IsClientControl="True" Text="Status">
            </px:PXCheckBox>
            
        </Template>
        <AutoSize Container="Window" Enabled="True" MinHeight="100" />
    </px:PXFormView>
</asp:Content>
<asp:Content ID="cont3" ContentPlaceHolderID="phG" runat="Server">
     <px:PXTab ID="tab" runat="server" Height="840px" Style="z-index: 100;" Width="100%">
        <Items>
            <px:PXTabItem Text="Programme Courses" BindingContext="form" RepaintOnDemand="false">
                <Template>
                    <px:PXGrid ID="gridProgrammeCourse" runat="server" DataSourceID="ds" Width="100%" SkinID="DetailsInTab" NoteIndicator="True" Style="left: 0px; top: 0px;" TabIndex="600" SyncPosition="True">
                        <AutoSize Enabled="True" />
                        <Mode InitNewRow="True" ></Mode>
                        
                        <EmptyMsg AnonFilteredAddMessage="No records found.
Try to change filter to see records here." AnonFilteredMessage="No records found.
Try to change filter to see records here." ComboAddMessage="No records found.
Try to change filter or modify parameters above to see records here." FilteredAddMessage="No records found.
Try to change filter to see records here." FilteredMessage="No records found.
Try to change filter to see records here." NamedComboAddMessage="No records found as '{0}'.
Try to change filter or modify parameters above to see records here." NamedComboMessage="No records found as '{0}'.
Try to change filter or modify parameters above to see records here." NamedFilteredAddMessage="No records found as '{0}'.
Try to change filter to see records here." NamedFilteredMessage="No records found as '{0}'.
Try to change filter to see records here." />
                        
                        <Levels>
                            <px:PXGridLevel DataMember="ProgrammeCourses">
                                <RowTemplate>
                                    <px:PXSegmentMask ID="edCourseID" runat="server" DataField="CourseID">
                                    </px:PXSegmentMask>
                                    <px:PXCheckBox ID="edStatus" runat="server" AlreadyLocalized="False" DataField="Status" ForceServerRendering="False" IsClientControl="True" Text="Status">
                                    </px:PXCheckBox>
                                    <px:PXTextEdit ID="edTermID" runat="server" AlreadyLocalized="False" DataField="TermID" IsClientControl="True">
                                    </px:PXTextEdit>
                                </RowTemplate>
                                <Columns>
                                    <px:PXGridColumn DataField="CourseID" Width="108px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="Status" TextAlign="Center" Type="CheckBox" Width="60px">
                                    </px:PXGridColumn>
                                    <px:PXGridColumn DataField="TermID" Width="96px">
                                    </px:PXGridColumn>
                                </Columns>
                            </px:PXGridLevel>
                        </Levels>
                    </px:PXGrid>
                </Template>
            </px:PXTabItem>
        </Items>
        </px:PXTab>
	</asp:Content>